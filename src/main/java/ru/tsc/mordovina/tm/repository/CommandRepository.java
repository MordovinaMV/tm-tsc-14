package ru.tsc.mordovina.tm.repository;

import ru.tsc.mordovina.tm.api.repository.ICommandRepository;
import ru.tsc.mordovina.tm.constant.ArgumentConst;
import ru.tsc.mordovina.tm.constant.TerminalConst;
import ru.tsc.mordovina.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Display system information..."
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Display Developer info..."
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Display List of commands..."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Display program version..."
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Display list commands..."
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Display list arguments..."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application..."
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show task list..."
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task..."
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks..."
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show project list..."
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project..."
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects..."
    );

    public static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Show task by id..."
    );

    public static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Show task by index..."
    );

    public static final Command TASK_SHOW_BY_NAME = new Command(
            TerminalConst.TASK_SHOW_BY_NAME, null,
            "Show task by name..."
    );

    public static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id..."
    );

    public static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index..."
    );

    public static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null,
            "Remove task by name..."
    );

    public static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update task by id..."
    );

    public static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index..."
    );

    public static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Show project by id..."
    );

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Show project by index..."
    );

    public static final Command PROJECT_SHOW_BY_NAME = new Command(
            TerminalConst.PROJECT_SHOW_BY_NAME, null,
            "Show project by name..."
    );

    public static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id..."
    );

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index..."
    );

    public static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null,
            "Remove project by name..."
    );

    public static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update project by id..."
    );

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index..."
    );

    public static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null,
            "Start project by id..."
    );

    public static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null,
            "Start project by index..."
    );

    public static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.PROJECT_START_BY_NAME, null,
            "Start project by name..."
    );

    public static final Command PROJECT_FINISH_BY_ID = new Command(
            TerminalConst.PROJECT_FINISH_BY_ID, null,
            "Finish project by id..."
    );

    public static final Command PROJECT_FINISH_BY_INDEX = new Command(
            TerminalConst.PROJECT_FINISH_BY_INDEX, null,
            "Finish project by index..."
    );

    public static final Command PROJECT_FINISH_BY_NAME = new Command(
            TerminalConst.PROJECT_FINISH_BY_NAME, null,
            "Finish project by name..."
    );

    public static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_ID, null,
            "Change project by id..."
    );

    public static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Change project by index..."
    );

    public static final Command PROJECT_CHANGE_STATUS_BY_NAME = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME, null,
            "Change project by name..."
    );

    public static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null,
            "Start task by id..."
    );

    public static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null,
            "Start task by index..."
    );

    public static final Command TASK_START_BY_NAME = new Command(
            TerminalConst.TASK_START_BY_NAME, null,
            "Start task by name..."
    );

    public static final Command TASK_FINISH_BY_ID = new Command(
            TerminalConst.TASK_FINISH_BY_ID, null,
            "Finish task by id..."
    );

    public static final Command TASK_FINISH_BY_INDEX = new Command(
            TerminalConst.TASK_FINISH_BY_INDEX, null,
            "Finish task by index..."
    );

    public static final Command TASK_FINISH_BY_NAME = new Command(
            TerminalConst.TASK_FINISH_BY_NAME, null,
            "Finish task by name..."
    );

    public static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_ID, null,
            "Change task by id..."
    );

    public static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_INDEX, null,
            "Change task by index..."
    );

    public static final Command TASK_CHANGE_STATUS_BY_NAME = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_NAME, null,
            "Change task by name..."
    );

    public static final Command TASK_LIST_BY_PROJECT_ID = new Command(
            TerminalConst.TASK_LIST_BY_PROJECT_ID, null,
            "Show task list by project id..."
    );

    public static final Command TASK_BIND_TO_PROJECT = new Command(
            TerminalConst.TASK_BIND_TO_PROJECT, null,
            "Bind task to project..."
    );

    public static final Command TASK_UNBIND_FROM_PROJECT = new Command(
            TerminalConst.TASK_UNBIND_FROM_PROJECT, null,
            "Unbind task from project..."
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, HELP, VERSION, COMMANDS, ARGUMENTS,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_SHOW_BY_NAME,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_NAME,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_START_BY_NAME,
            PROJECT_FINISH_BY_ID, PROJECT_FINISH_BY_INDEX, PROJECT_FINISH_BY_NAME,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_CHANGE_STATUS_BY_NAME,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_START_BY_NAME,
            TASK_FINISH_BY_ID, TASK_FINISH_BY_INDEX, TASK_FINISH_BY_NAME,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX, TASK_CHANGE_STATUS_BY_NAME,
            TASK_LIST_BY_PROJECT_ID, TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT,
            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
