package ru.tsc.mordovina.tm.enumerated;

import ru.tsc.mordovina.tm.comparator.ComparatorByCreated;
import ru.tsc.mordovina.tm.comparator.ComparatorByName;
import ru.tsc.mordovina.tm.comparator.ComparatorByStartDate;
import ru.tsc.mordovina.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    START_DATE("Sort by date start", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.comparator = comparator;
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}

