package ru.tsc.mordovina.tm.service;

import ru.tsc.mordovina.tm.api.repository.IProjectRepository;
import ru.tsc.mordovina.tm.api.service.IProjectService;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return projectRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(Integer index) {
        return projectRepository.existsByIndex(index);
    }

    @Override
    public Project startById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.finishByName(name);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (status == null) return null;
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) return null;
        return projectRepository.changeStatusByName(name, status);
    }

}
