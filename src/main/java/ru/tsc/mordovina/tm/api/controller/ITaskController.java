package ru.tsc.mordovina.tm.api.controller;

import ru.tsc.mordovina.tm.model.Task;

public interface ITaskController {

    void showById();

    void showByIndex();

    void showByName();

    void createTask();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateByIndex();

    void updateById();

    void showTasks();

    void clearTasks();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

    void showTask(Task task);

}
